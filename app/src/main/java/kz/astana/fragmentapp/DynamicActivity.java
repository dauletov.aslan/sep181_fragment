package kz.astana.fragmentapp;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class DynamicActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dynamic);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        DynamicFragment dynamicFragment = new DynamicFragment();
        fragmentTransaction.add(R.id.container, dynamicFragment, DynamicFragment.class.getName());
//        fragmentTransaction.commit();

        fragmentTransaction.remove(dynamicFragment);
        fragmentTransaction.commit();
    }
}